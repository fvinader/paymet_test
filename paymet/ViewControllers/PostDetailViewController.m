//
//  PostDetailViewController.m
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import "PostDetailViewController.h"

@interface PostDetailViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation PostDetailViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

#pragma mark - View Configuration

- (void)configureView {
    [self.webView loadHTMLString:self.post.content baseURL:nil];
}

@end
