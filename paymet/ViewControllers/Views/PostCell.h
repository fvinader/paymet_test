//
//  PostCell.h
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UILabel *title;
@end
