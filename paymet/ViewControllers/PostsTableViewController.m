//
//  PostsTableViewController.m
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import "PostsTableViewController.h"
#import "Posts.h"
#import "PostCell.h"
#import "UIImageView+AFNetworking.h"
#import "PostDetailViewController.h"

static NSString * const SeguePostDetail = @"PostDetailSegue";
static NSString * const CellPostIdentifier = @"PostCell";

@interface PostsTableViewController ()
@property (nonatomic, strong) NSArray *posts;
@end

@implementation PostsTableViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"PayMet Test";

    self.clearsSelectionOnViewWillAppear = YES;
    [self loadPosts];
}

#pragma mark - Data Management

- (void)loadPosts {
    [Posts getPostsWithBlock:^(NSString *error, NSArray *posts) {
        if (!error) {
            self.posts = posts;
            [self.tableView reloadData];
        }else {
            
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.posts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellPostIdentifier forIndexPath:indexPath];
    
    Post *post = self.posts[indexPath.row];
    cell.title.text = post.title;
    
    NSURLRequest *request = [NSURLRequest requestWithURL: [NSURL URLWithString:post.imageURL]];
    __weak UITableView *weakTableView = tableView;
    __weak UITableViewCell *weakCell = cell;
    __weak UIImageView *weakImageView = cell.mainImage;
    [cell.mainImage setImageWithURLRequest:request
                          placeholderImage:[UIImage imageNamed:@"logo_paymet"]
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       weakImageView.image = image;
                                       if ([weakTableView.visibleCells containsObject:weakCell]) {
                                           [weakTableView reloadRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationNone];
                                       }
                                   } failure:nil];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:SeguePostDetail sender:self.posts[indexPath.row]];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PostDetailViewController *postDetailVC = [segue destinationViewController];
    postDetailVC.post = sender;
}


@end
