//
//  PostDetailViewController.h
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface PostDetailViewController : UIViewController
@property (nonatomic,strong) Post *post;
@end
