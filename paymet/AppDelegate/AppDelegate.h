//
//  AppDelegate.h
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

