//
//  PostsProvider.h
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostsProvider : NSObject

+ (void)getPostsWithBlock: (void (^) (NSError *error, NSArray *posts)) block;

@end
