//
//  PostsProvider.m
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import "PostsProvider.h"
#import "AFHTTPSessionManager.h"

static NSString * const BaseURLString = @"https://public-api.wordpress.com/rest/v1.1/";

@implementation PostsProvider

+ (void)getPostsWithBlock: (void (^) (NSError *error, NSArray *posts)) block {
    
    NSURL *url = [NSURL URLWithString:BaseURLString];
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];

    AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url sessionConfiguration:sessionConfiguration];
    
    NSURLSessionDataTask *dataTask = [sessionManager GET:@"sites/en.blog.wordpress.com/posts" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"Success: /n%@", responseObject);
        NSArray *posts = responseObject[@"posts"];
        
        block(nil, posts);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error.description);
        block(error, nil);
    }];
    [dataTask resume];
}

@end
