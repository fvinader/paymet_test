//
//  Posts.m
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import "Posts.h"
#import "PostsProvider.h"

@implementation Posts

+ (void)getPostsWithBlock: (void (^) (NSString *error, NSArray *posts)) block {
    [PostsProvider getPostsWithBlock:^(NSError *error, NSArray *posts) {
        if (!error) {
            NSMutableArray *parsedPosts = [[NSMutableArray alloc] init];
            for (NSDictionary *postDict in posts) {
                [parsedPosts addObject:[[Post alloc] initWithDictionary:postDict]];
            }
            block(nil, parsedPosts);
        }else {
            block(error.description, nil);
        }
    }];
}

@end
