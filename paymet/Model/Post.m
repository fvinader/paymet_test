//
//  Post.m
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import "Post.h"

@implementation Post

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _title = dict[@"title"];
        _content = dict[@"content"];
        NSDictionary *thumbnailDict = dict[@"post_thumbnail"];
        _imageURL = thumbnailDict[@"URL"];
    }
    return self;
}

@end
