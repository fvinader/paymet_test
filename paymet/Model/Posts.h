//
//  Posts.h
//  paymet
//
//  Created by Fernando on 24/12/15.
//  Copyright © 2015 fvinader. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"

@interface Posts : NSObject

+ (void)getPostsWithBlock: (void (^) (NSString *error, NSArray *posts)) block;

@end
